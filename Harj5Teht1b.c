#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


    double laske_joulukuun_palkka(
    double kk_palkka, //henkilon kuukausipalkka
    int palvelusaika) //henkilon palvelusaika yrityksessa
    {

    double bonus;
    double result=0;
    if (kk_palkka >=1500 && kk_palkka<=3500) {
    if (palvelusaika >= 8) bonus = kk_palkka;
    else if (palvelusaika >= 5) bonus = kk_palkka*0.7;
    else if (palvelusaika >= 3) bonus = kk_palkka*0.5;
    else bonus = 0;
    result = kk_palkka + bonus;

    }
    //printf("%lf\n", result);
    return result;
    }

//testiajuri
bool test_laske_joulukuun_palkka() {
double palkka;
bool test_ok = true;

//testitapaus 1
palkka = laske_joulukuun_palkka(1500, 3);
test_ok = test_ok && (abs(palkka-2250)<0.01);

//testitapaus 2
palkka = laske_joulukuun_palkka(3500, 5);
test_ok = test_ok && (abs(palkka-5950)<0.01);

//testitapaus 3
palkka = laske_joulukuun_palkka(4000, 4);
test_ok = test_ok && (abs(palkka-0)<0.01);

return test_ok;
}

int main(){

printf("%d\n",test_laske_joulukuun_palkka());

    return 0;
}
